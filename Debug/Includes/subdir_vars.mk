################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Includes/config.c \
../Includes/i2c_func.c \
../Includes/uart_func.c 

C_DEPS += \
./Includes/config.d \
./Includes/i2c_func.d \
./Includes/uart_func.d 

OBJS += \
./Includes/config.obj \
./Includes/i2c_func.obj \
./Includes/uart_func.obj 

OBJS__QUOTED += \
"Includes\config.obj" \
"Includes\i2c_func.obj" \
"Includes\uart_func.obj" 

C_DEPS__QUOTED += \
"Includes\config.d" \
"Includes\i2c_func.d" \
"Includes\uart_func.d" 

C_SRCS__QUOTED += \
"../Includes/config.c" \
"../Includes/i2c_func.c" \
"../Includes/uart_func.c" 


