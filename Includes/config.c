/*
 * config.c
 *
 *  Created on: 03/04/2018
 *      Author: hydro
 */

//Includes
#include "config.h"

void config(){

    WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
    P1OUT &= ~BIT0;                           // P1.0 = 0
    P1DIR |= BIT0 + BIT4;                     // P1.0 y P1.4 output

#if reloj == 1
  BCSCTL1 = CALBC1_1MHZ;   // Set DCO to 1MHz
  DCOCTL = CALDCO_1MHZ;    // Set DCO to 1MHz
#endif

#if reloj == 16
  BCSCTL1 = CALBC1_16MHZ;      //Coloca el DCO a 16 MHz
  DCOCTL = CALDCO_16MHZ;
#endif

  //uart_conf();
  i2c_conf();

}

void dormir()
{
    __bis_SR_register(LPM0_bits + GIE); // Enter LPM0, interrupts enabled
}
