/*
 * masterSetup.h
 *
 *  Created on: 03/04/2018
 *      Author: hydro
 */

#ifndef INCLUDES_MASTERSETUP_H_
#define INCLUDES_MASTERSETUP_H_

//Definiciones
#define reloj 1     //1 Mhz
//#define reloj 16     //16Mhz

//I2C


//UART Configuraciones
#define baudios 9600        //9600
//#define baudios 57600     //57600
//#define baudios 115200    //115200

#define rx_A0_int           //habilitar interrupciones por recepción de Serial
#define rx_B0_int           //habilitar interrupciones por recepción de I2C


//includes
#include "msp430g2553.h"

#include "config.h"
#include "uart_func.h"
#include "i2c_func.h"

#endif /* INCLUDES_MASTERSETUP_H_ */
