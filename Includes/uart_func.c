/*
 * uart_func.c
 *
 *  Created on: 06/09/2017
 *      Author: hydro
 */

#include "uart_func.h"

char count = 0;
char eraserCount = 0;
char data[4];
char dat;

const int vecLen = 1025;
const char vecSeg[vecLen]={129,251,0,0,255,255,255,0,1,3,4,3,255,251,247,247,249,254,4,9,12,10,14,11,4,248,234,222,218,227,249,20,43,53,46,25,0,233,220,217,221,229,240,253,12,27,38,40,31,12,246,229,223,230,242,0,8,8,2,251,249,253,6,16,22,22,10,255,241,230,227,232,244,2,10,20,21,17,10,2,252,247,245,244,245,248,253,2,6,9,10,8,4,0,253,251,251,251,252,254,255,1,2,2,3,2,2,1,0,255,254,254,253,254,254,0,1,2,2,2,2,1,255,254,254,254,254,0,1,2,2,1,255,253,252,253,0,4,8,11,9,3,250,239,232,232,241,2,21,36,40,30,10,240,219,209,216,234,3,25,36,35,24,8,249,240,237,240,245,251,255,1,3,5,7,8,8,5,1,253,250,248,249,251,254,1,3,4,4,3,1,0,0,255,254,254,254,254,255,0,0,1,1,1,1,1,0,0,255,255,254,254,255,0,1,1,2,1,0,0,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,254,254,254,0,2,6,9,10,6,254,243,234,231,238,253,17,34,41,33,12,240,217,207,214,236,8,32,42,38,21,0,238,230,232,240,251,4,9,9,7,4,2,1,1,1,0,254,252,251,252,253,0,2,3,3,2,1,255,254,254,255,1,2,2,1,255,253,251,252,254,0,3,5,6,4,1,254,251,251,252,254,0,2,3,3,2,1,255,255,255,255,255,255,255,255,255,0,0,1,2,2,2,1,0,254,253,253,253,253,254,0,3,7,9,9,6,254,245,237,234,238,251,10,28,35,30,12,245,224,215,220,238,6,25,35,31,17,0,242,235,237,244,253,4,7,7,4,1,0,0,0,1,1,0,255,253,253,253,255,0,1,2,1,0,0,0,1,1,2,1,0,254,252,251,252,255,2,5,6,5,3,255,252,251,251,252,255,1,3,4,3,1,0,255,254,254,254,255,0,0,0,0,0,0,1,1,1,1,0,255,255,254,253,254,254,0,1,3,4,5,5,4,1,253,248,244,243,246,254,8,18,23,21,10,250,235,227,229,241,1,17,25,24,15,2,246,240,241,246,253,3,6,6,3,1,255,255,0,1,2,1,255,253,252,253,255,1,3,3,2,1,255,254,255,0,1,2,1,255,253,252,253,255,2,4,5,4,1,254,252,251,252,254,0,2,3,3,3,1,0,254,253,253,254,255,1,2,3,2,1,0,254,254,255,255,0,1,1,0,255,254,255,255,1,2,2,3,2,2,0,255,252,250,249,249,252,2,9,14,14,10,1,247,239,237,242,252,7,15,17,10,4,252,246,245,248,253,2,4,5,3,1,255,255,255,0,1,1,0,255,254,254,255,0,1,1,1,0,255,255,255,1,2,2,1,255,253,253,253,255,1,3,4,3,1,255,253,253,253,255,1,2,3,2,1,0,254,253,253,254,255,0,2,2,2,1,0,255,254,254,255,0,1,1,1,0,255,254,255,255,0,1,2,2,2,2,1,255,254,251,250,250,252,1,6,10,11,9,2,250,244,242,245,252,4,10,12,10,4,254,249,248,250,254,1,3,3,2,0,255,255,255,1,1,1,0,255,255,255,255,0,1,1,0,255,255,255,0,1,2,2,1,255,254,253,254,255,1,2,3,2,0,255,254,254,254,255,1,2,2,2,1,0,254,253,253,254,255,1,2,3,3,1,0,254,253,254,255,0,1,1,1,0,255,255,255,255,0,0,1,1,1,1,1,1,0,254,252,251,251,253,1,6,9,9,6,0,250,246,245,248,254,4,8,9,6,2,254,251,251,253,0,1,1,0,255,254,255,0,1,2,2,1,255,254,253,254,255,0,1,1,1,0,255,255,0,1,1,1,0,255,254,254,254,255,1,1,1,1,0,255,254,255,0,1,2,2,1,0,254,253,253,253,255,0,2,3,3,2,1,255,254,253,253,255,0,2,2,2,1,255,254,254,255,0,1,1,1,1,1,1,1,0,0,254,252,251,252,255,3,7,9,7,3,253,248,246,247,251,1,6,8,7,4,0,254,253,253,254,255,0,255,255,255,0,1,2,3,2,1,255,253,253,254,255,1,2,2,1,0,255,254,255,0,1,2,1,0,255,255,254,255,255,0,0,0,0,0,0,1,2,2,1,0,255,254,253,253,254,0,1,2,2,2,1,0,0,255,254,254,255,255,0,1,1,1,1,0,255,255,255,0,0,1,0,0,0,0,1,1,1,255,253,251,251,253,0,4,7,8,6,1,252,248,246,248,253,2,6,8,7,4,0,253,251};

//char *bufRXpnt;                       // uartRX buffer pointer

void uart_conf()
{
    P1SEL = BIT1 + BIT2 ;    // P1.1 = RXD, P1.2=TXD
    P1SEL2 = BIT1 + BIT2 ;   // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 |= UCSSEL_2;    // Use SMCLK

#ifdef LORA
    UCA0BR0 = 21;   // 16MHz 57600
    UCA0BR1 = 1;    // 16MHz 57600
    UCA0MCTL = UCBRS_6; // Modulation UCBRSx = 1
#endif

#ifdef SF
    UCA0BR0 = 130;   // 16MHz 9600
    UCA0BR1 = 6;    // 16MHz 9600
    UCA0MCTL = UCBRS_5; // Modulation UCBRSx = 1
#endif



#if reloj == 1 && baudios == 9600
  UCA0BR0 = 104;           // Set baud rate to 9600 with 1MHz clock (Data Sheet 15.3.13)
  UCA0BR1 = 0;             // Set baud rate to 9600 with 1MHz clock
   UCA0MCTL = UCBRS0;       // Modulation UCBRSx = 1
#endif

#if reloj == 16 && baudios == 9600
  UCA0BR0 = 130;                            // 16MHz 9600
  UCA0BR1 = 6;                              // 16MHz 9600
  UCA0MCTL = UCBRS_6;                        // Modulation UCBRSx = 1
#endif

#if reloj == 16 && baudios == 115200
  UCA0BR0 = 138;                            // 16MHz 115200
  UCA0BR1 = 0;                              // 16MHz 115200
  UCA0MCTL = UCBRS_6;                        // Modulation UCBRSx = 1
#endif

    UCA0CTL1 &= ~UCSWRST;
#ifdef rx_A0_int
    IE2 |= UCA0RXIE;         // Enable USCI_A0 RX interrupt
#endif
}

void UARTSendArray(char *TxArray, int ArrayLength){
  // Send number of bytes Specified in ArrayLength in the array at using the hardware UART 0
  // Example usage: UARTSendArray("Hello", 5);
  // int data[2]={1023, 235};
  // UARTSendArray(data, 4);  // Note because the UART transmits bytes it is necessary to send two bytes for each integer hence the data length is twice the array length

    while(ArrayLength--){             // Loop until StringLength == 0 and post decrement
    while(!(IFG2 & UCA0TXIFG));      // Wait for TX buffer to be ready for new data
    UCA0TXBUF = *TxArray++;           //Write the character at the location specified by the pointer and post increment
  }
}

void UARTSendChar(char *TxChar){
  // Send number of bytes Specified in ArrayLength in the array at using the hardware UART 0
  // Example usage: UARTSendArray('A');
  while(!(IFG2 & UCA0TXIFG));      // Wait for TX buffer to be ready for new data
  UCA0TXBUF = TxChar;           //Write the character at the location specified py the pointer
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{

#ifdef test
    if(IFG2 & UCA0RXIFG)
    {
        data[count] = UCA0RXBUF;    //Lectura de datos recibido por UART
    }
    count++; //incrementa contador de datos enviados
    if(count == 4)//Si el contador es igual a 4
    {
        UARTSendArray(data,4);//env�a la trama de datos acumulada
        count = 0;//resetea el contador
        if(data[0] == 'o' && data[1] == 'k' && data[2] == '\r' && data[3] == '\n')//Si se recibi� un "ok\r\n"
        {
            __bic_SR_register_on_exit(LPM0_bits);//Despertar
        }
        for(eraserCount = 0; eraserCount <= 4; eraserCount++)//borrar la variable que almacena el dato
        {
            data[eraserCount] = 0;
        }
    }
#endif

#ifdef LORA
    if(IFG2 & UCA0RXIFG)
    {
        data[count] = UCA0RXBUF;    //Lectura de datos recibido por UART
    }
    count++; //incrementa contador de datos enviados
    if(count == 4)//Si el contador es igual a 4
    {
        UARTSendArray(data,4);//env�a la trama de datos acumulada
        count = 0;//resetea el contador
        if(data[0] == 'o' && data[1] == 'k' && data[2] == '\r' && data[3] == '\n')//Si se recibi� un "ok\r\n"
        {
            __bic_SR_register_on_exit(LPM0_bits);//Despertar
        }
        for(eraserCount = 0; eraserCount <= 4; eraserCount++)//borrar la variable que almacena el dato
        {
            data[eraserCount] = 0;
        }
    }
#endif

#ifdef SF
    dat = UCA0RXBUF;
//    UARTSendChar(dat);
//    UARTSendArray("\r\n",2);
    __bic_SR_register_on_exit(LPM0_bits);//Despertar
#endif


    volatile char data;
    data = UCA0RXBUF;
      switch(data){
      case 'S':
        {
          UARTSendArray(vecSeg, vecLen);
          P1OUT &= ~BIT6;            // Set P1.0
        }
        break;
      default:
        {
          UARTSendArray("Unknown Command: ", 17);
          UARTSendChar(data);
          UARTSendArray("\r\n", 2);
        }
        break;
      }
}








