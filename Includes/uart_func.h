/*
 * uart_func.h
 *
 *  Created on: 06/09/2017
 *      Author: hydro
 */

#ifndef UART_FUNC_H_
#define UART_FUNC_H_

#include "masterSetup.h"

void uart_conf();//Funci�n para configurar UART
void UARTSendArray(char *TxArray, int ArrayLength); //Funci�n para enviar cadenas de caracteres por UART
void UARTSendChar(char *TxChar);    //Funci�n para enviar un s�lo caracter por UART


#endif /* UART_FUNC_H_ */
